# Mindstorms Robot Inventory Fake Library

This is a fake library for the development with the LEGO Mindstorms Robot Inventor set (51515). It's only purpose is to be able to write code without getting bombarded by errors from you IDE and to simplify development by allowing you IDE to provide you with autocompletion for the code.

Remember: the code you write has to be executed on the hub! This library does not contain any functionality!

## Getting started

Clone or download the repo into your project into a folder called "mindstorms". You can also install it through pip/pipenv/… from the repo.

### Cloning the repo

- with HTTP: `git clone https://gitlab.com/twinter/mindstorms-robot-inventory-fake-library.git mindstorms`
- with SSH: `git clone git@gitlab.com:twinter/mindstorms-robot-inventory-fake-library.git mindstorms`
