from typing import Literal, Tuple

from _MSHub import _PortOptions
from _default_error import raise_not_implemented_error


class ColorSensor:
    def __init__(self, port=_PortOptions):
        pass
    
    def get_color(self) -> Literal['black', 'blue', 'cyan', 'green', 'red', 'violet', 'white', 'yellow', None]:
        raise_not_implemented_error()
    
    def get_ambient_light(self) -> int:
        raise_not_implemented_error()
    
    def get_reflected_light(self) -> int:
        raise_not_implemented_error()
    
    def get_rgb_intensity(self) -> Tuple[int, int, int, int]:
        raise_not_implemented_error()
    
    def get_red(self) -> int:
        raise_not_implemented_error()
    
    def get_green(self) -> int:
        raise_not_implemented_error()
    
    def get_blue(self) -> int:
        raise_not_implemented_error()
    
    def wait_until_color(self,
            color: Literal['black', 'blue', 'cyan', 'green', 'red', 'violet', 'white', 'yellow', None]
    ):
        raise_not_implemented_error()
    
    def wait_for_new_color(self) -> Literal['black', 'blue', 'cyan', 'green', 'red', 'violet', 'white', 'yellow', None]:
        raise_not_implemented_error()
    
    def light_up_all(self, brightness: int = 100):
        raise_not_implemented_error()
    
    def light_up(self, light_1: int = 100, light_2: int = 100, light_3: int = 100):
        raise_not_implemented_error()
