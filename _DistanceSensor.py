from typing import Literal, Union

from _MSHub import _PortOptions
from _default_error import raise_not_implemented_error


class DistanceSensor:
    def __init__(self, port=_PortOptions):
        pass
    
    def get_distance_cm(self, short_range: bool = False) -> Union[int, None]:
        raise_not_implemented_error()
    
    def get_distance_inches(self, short_range: bool = False) -> Union[int, None]:
        raise_not_implemented_error()
    
    def get_distance_percentage(self, short_range: bool = False) -> Union[int, None]:
        raise_not_implemented_error()
    
    def wait_for_distance_farther_than(self, distance: float,
            unit: Literal['cm', 'in', '%'] = 'cm',
            short_range: bool = False
    ):
        raise_not_implemented_error()
    
    def wait_for_distance_closer_than(self, distance: float,
            unit: Literal['cm', 'in', '%'] = 'cm',
            short_range: bool = False
    ):
        raise_not_implemented_error()
    
    def light_up_all(self, brightness: int = 100):
        raise_not_implemented_error()
    
    def light_up(self, right_top: int = 100, left_top: int = 100, right_bottom: int = 100, left_bottom: int = 100):
        raise_not_implemented_error()
