from typing import Literal

from _MSHub import _PortOptions
from _default_error import raise_not_implemented_error


class ForceSensor:
    def __init__(self, port=_PortOptions):
        pass
    
    def is_pressed(self) -> bool:
        raise_not_implemented_error()
        
    def get_force_newton(self) -> int:
        raise_not_implemented_error()
        
    def get_force_percentage(self) -> int:
        raise_not_implemented_error()
        
    def wait_until_pressed(self):
        raise_not_implemented_error()
        
    def wait_until_released(self):
        raise_not_implemented_error()
