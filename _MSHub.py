from typing import Literal, List

from mindstorms._default_error import raise_not_implemented_error

_PortOptions = Literal['A', 'B', 'C', 'D', 'E', 'F']


class _Button:
    def wait_until_pressed(self):
        raise_not_implemented_error()

    def wait_until_released(self):
        raise_not_implemented_error()

    def was_pressed(self) -> bool:
        raise_not_implemented_error()

    def is_pressed(self) -> bool:
        raise_not_implemented_error()

    def is_released(self) -> bool:
        raise_not_implemented_error()


class _StatusLight:
    def on(self, color: Literal['azure', 'black', 'blue', 'cyan', 'green', 'orange', 'pink', 'red', 'violet',
                                'white', 'yellow'] = 'white'):
        raise_not_implemented_error()

    def off(self):
        raise_not_implemented_error()


class _LightMatrix:
    def set_pixel(self, x: int, y: int, brightness: int = 100):
        raise_not_implemented_error()

    def show_image(self, image: Literal['ANGRY', 'ARROW_E', 'ARROW_N', 'ARROW_NE', 'ARROW_NW', 'ARROW_S', 'ARROW_SE',
                                        'ARROW_SW', 'ARROW_W', 'ASLEEP', 'BUTTERFLY', 'CHESSBOARD', 'CLOCK1', 'CLOCK10',
                                        'CLOCK11', 'CLOCK12', 'CLOCK2', 'CLOCK3', 'CLOCK4', 'CLOCK5', 'CLOCK6',
                                        'CLOCK7', 'CLOCK8', 'CLOCK9', 'CONFUSED', 'COW', 'DIAMOND', 'DIAMOND_SMALL',
                                        'DUCK', 'FABULOUS', 'GHOST', 'GIRAFFE', 'GO_RIGHT', 'GO_LEFT', 'GO_UP',
                                        'GO_DOWN', 'HAPPY', 'HEART', 'HEART_SMALL', 'HOUSE', 'MEH', 'MUSIC_CROTCHET',
                                        'MUSIC_QUAVER', 'MUSIC_QUAVERS', 'NO', 'PACMAN', 'PITCHFORK', 'RABBIT',
                                        'ROLLERSKATE', 'SAD', 'SILLY', 'SKULL', 'SMILE', 'SNAKE', 'SQUARE',
                                        'SQUARE_SMALL', 'STICKFIGURE', 'SURPRISED', 'SWORD', 'TARGET', 'TORTOISE',
                                        'TRIANGLE', 'TRIANGLE_LEFT', 'TSHIRT', 'UMBRELLA', 'XMAS', 'YES'],
                   brightness: int = 100):
        raise_not_implemented_error()

    def write(self, text: str):
        raise_not_implemented_error()

    def off(self):
        raise_not_implemented_error()

    def show(self, pixels: str):
        raise_not_implemented_error()

    def play_animation(self, animation: List[str], fps: int = 2.5, effect: str = 'direct', clear: bool = False):
        raise_not_implemented_error()

    def start_animation(self, animation: List[str], fps: int = 2.5, loop: bool = False, effect: str = 'direct',
                        clear: bool = False):
        raise_not_implemented_error()

    def set_orientation(self, orientation: Literal['upright', 'left', 'upside down', 'right'] = 'upright'):
        raise_not_implemented_error()

    def direction(self, direction: Literal['clockwise', 'counterclockwise'] = 'clockwise'):
        raise_not_implemented_error()

    def get_orientation(self) -> Literal['upright', 'left', 'upside down', 'right']:
        raise_not_implemented_error()


class _Speaker:
    def beep(self, note: int = 60, seconds: float = 0.2, volume: int = None):
        raise_not_implemented_error()

    def start_beep(self, note: int = 60, volume: int = None):
        raise_not_implemented_error()

    def stop(self):
        raise_not_implemented_error()

    def play_sound(self, name: Literal['1234', 'Activate', 'Affirmative', 'Bowling', 'Brick Eating', 'Celebrate',
                                       'Chuckle', 'Countdown', 'Countdown Tick', 'Damage', 'Deactivate', 'Delivery',
                                       'Dizzy', 'Error', 'Explosion', 'Exterminate', 'Fire', 'Goal', 'Goodbye', 'Grab',
                                       'Hammer', 'Hello', 'Hi', 'Hi 5', 'Hit', 'Horn', 'Humming', 'Hydraulics Down',
                                       'Hydraulics Up', 'Initialize', 'Kick', 'Laser', 'Laugh', 'Like',
                                       'Mission Accomplished', 'No', 'Ouch', 'Ping', 'Play', 'Punch', 'Reverse',
                                       'Revving', 'Sad', 'Scanning', 'Scared', 'Seek and Destroy', 'Shake', 'Shooting',
                                       'Shut Down', 'Slam Dunk', 'Strike', 'Success Chime', 'Tadaa', 'Target Acquired',
                                       'Target Destroyed', 'Whirl', 'Wow', 'Yes', 'Yipee', 'Yuck'],
                   volume: int = None):
        raise_not_implemented_error()

    def start_sound(self, name: Literal['1234', 'Activate', 'Affirmative', 'Bowling', 'Brick Eating', 'Celebrate',
                                        'Chuckle', 'Countdown', 'Countdown Tick', 'Damage', 'Deactivate', 'Delivery',
                                        'Dizzy', 'Error', 'Explosion', 'Exterminate', 'Fire', 'Goal', 'Goodbye', 'Grab',
                                        'Hammer', 'Hello', 'Hi', 'Hi 5', 'Hit', 'Horn', 'Humming', 'Hydraulics Down',
                                        'Hydraulics Up', 'Initialize', 'Kick', 'Laser', 'Laugh', 'Like',
                                        'Mission Accomplished', 'No', 'Ouch', 'Ping', 'Play', 'Punch', 'Reverse',
                                        'Revving', 'Sad', 'Scanning', 'Scared', 'Seek and Destroy', 'Shake', 'Shooting',
                                        'Shut Down', 'Slam Dunk', 'Strike', 'Success Chime', 'Tadaa', 'Target Acquired',
                                        'Target Destroyed', 'Whirl', 'Wow', 'Yes', 'Yipee', 'Yuck'],
                    volume: int = None):
        raise_not_implemented_error()

    def get_volume(self) -> int:
        raise_not_implemented_error()

    def set_volume(self, volume: int = 100):
        raise_not_implemented_error()


_Orientation = Literal['front', 'back', 'up', 'down', 'leftside', 'rightside']


class _MotionSensor:
    def was_gesture(self, gesture: Literal['shaken', 'tapped', 'doubletapped', 'falling']) -> bool:
        raise_not_implemented_error()

    def wait_for_new_gesture(self) -> Literal['shaken', 'tapped', 'doubletapped', 'falling']:
        raise_not_implemented_error()

    def wait_for_new_orientation(self) -> _Orientation:
        raise_not_implemented_error()

    def get_orientation(self) -> _Orientation:
        raise_not_implemented_error()

    def get_gesture(self) -> Literal['shaken', 'tapped', 'doubletapped', 'falling', None]:
        raise_not_implemented_error()

    def get_pitch_angle(self) -> int:
        raise_not_implemented_error()

    def get_roll_angle(self) -> int:
        raise_not_implemented_error()

    def get_yaw_angle(self) -> int:
        raise_not_implemented_error()

    def reset_yaw_angle(self):
        raise_not_implemented_error()


class MSHub:
    def __init__(self):
        self.left_button = _Button()
        self.right_button = _Button()
        self.speaker = _Speaker()
        self.light_matrix = _LightMatrix()
        self.status_light = _StatusLight()
        self.motion_sensor = _MotionSensor()

    @property
    def PORT_A(self):
        raise_not_implemented_error()

    @property
    def PORT_B(self):
        raise_not_implemented_error()

    @property
    def PORT_C(self):
        raise_not_implemented_error()

    @property
    def PORT_D(self):
        raise_not_implemented_error()

    @property
    def PORT_E(self):
        raise_not_implemented_error()

    @property
    def PORT_F(self):
        raise_not_implemented_error()
