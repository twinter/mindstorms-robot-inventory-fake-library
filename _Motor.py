from typing import Literal

from _MSHub import _PortOptions
from _default_error import raise_not_implemented_error


class Motor:
    def __init__(self, port=_PortOptions):
        pass
    
    def run_to_position(self, degrees: int,
            direction: Literal['shortest path', 'clockwise', 'counterclockwise'] = 'shortest path',
            speed: int = None
    ):
        raise_not_implemented_error()
    
    def run_to_degrees_counted(self, degrees: int, speed: int=None):
        raise_not_implemented_error()
        
    def run_for_degrees(self, degrees: int, speed: int=None):
        raise_not_implemented_error()
        
    def run_for_rotations(self, rotations: float, speed: int=None):
        raise_not_implemented_error()
        
    def run_for_seconds(self, seconds: float, speed: int=None):
        raise_not_implemented_error()
        
    def start(self, speed: int=None):
        raise_not_implemented_error()
        
    def start_at_power(self, power: int):
        raise_not_implemented_error()
        
    def stop(self):
        raise_not_implemented_error()
        
    def get_speed(self) -> int:
        raise_not_implemented_error()
        
    def get_position(self) -> int:
        raise_not_implemented_error()
        
    def get_degrees_counted(self) -> int:
        raise_not_implemented_error()
        
    def get_default_speed(self) -> int:
        raise_not_implemented_error()
        
    def was_interrupted(self) -> bool:
        raise_not_implemented_error()
        
    def was_stalled(self) -> bool:
        raise_not_implemented_error()
        
    def set_degrees_counted(self, degrees_counted: int):
        raise_not_implemented_error()
        
    def set_default_speed(self, default_speed: int):
        raise_not_implemented_error()
        
    def set_stop_action(self, action: Literal['coast', 'brake', 'hold']='coast'):
        raise_not_implemented_error()
        
    def set_stall_detection(self, stop_when_stalled: bool=True):
        raise_not_implemented_error()
