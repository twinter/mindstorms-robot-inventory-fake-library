from typing import Literal

from _MSHub import _PortOptions
from _default_error import raise_not_implemented_error


class MotorPair:
    def __init__(self, port1: _PortOptions, port2: _PortOptions):
        pass
    
    def move(self, amount: float,
            unit: Literal['cm', 'in', 'rotations', 'degrees', 'seconds'] = 'cm',
            steering: int = 0,
            speed: int = None):
        raise_not_implemented_error()
    
    def start(self, steering: int = 0, speed: int = None):
        raise_not_implemented_error()
    
    def stop(self):
        raise_not_implemented_error()
    
    def move_tank(self, amount: float,
            unit: Literal['cm', 'in', 'rotations', 'degrees', 'seconds'] = 'cm',
            left_speed: int = None, right_speed: int = None):
        raise_not_implemented_error()
    
    def start_tank(self, left_speed: int = None, right_speed: int = None):
        raise_not_implemented_error()
    
    def start_at_power(self, power: int, steering: int = 0):
        raise_not_implemented_error()
    
    def start_tank_at_power(self, left_power: int, right_power: int):
        raise_not_implemented_error()
    
    def get_default_speed(self) -> int:
        raise_not_implemented_error()
    
    def set_motor_rotation(self, amount: float = 17.6, unit: Literal['cm', 'in'] = 'cm'):
        raise_not_implemented_error()
        
    def set_default_speed(self, default_speed: int):
        raise_not_implemented_error()
        
    def set_stop_action(self, action: Literal['coast', 'brake', 'hold']='coast'):
        raise_not_implemented_error()
