def raise_not_implemented_error():
    raise NotImplementedError('You have to execute your code on the control brick!')
