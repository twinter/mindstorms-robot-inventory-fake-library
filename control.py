from typing import Callable, Any

from mindstorms._default_error import raise_not_implemented_error


def wait_for_seconds(seconds: float):
    raise_not_implemented_error()


def wait_until(get_value_function: Callable, operator_function: Callable, target_value: Any):
    raise_not_implemented_error()


class Timer:
    def reset(self):
        raise_not_implemented_error()

    def now(self) -> int:
        raise_not_implemented_error()
