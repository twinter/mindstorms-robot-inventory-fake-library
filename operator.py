from typing import Any

from _default_error import raise_not_implemented_error

def greater_than(a: Any, b: Any) -> bool:
    raise_not_implemented_error()
    
def greater_than_or_equal_to(a: Any, b: Any) -> bool:
    raise_not_implemented_error()
    
def less_than(a: Any, b: Any) -> bool:
    raise_not_implemented_error()
    
def less_than_or_equal_to(a: Any, b: Any) -> bool:
    raise_not_implemented_error()
    
def equal_to(a: Any, b: Any) -> bool:
    raise_not_implemented_error()
    
def not_equal_to(a: Any, b: Any) -> bool:
    raise_not_implemented_error()
    

